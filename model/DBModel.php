<?php
include_once("IModel.php");
include_once("Book.php");
include_once __DIR__ . '/../TestDBProps.php';

/** The Model is the class holding data about a collection of books. 
 * @author Adrian Holmeset
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
 
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
	
	  
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
				
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
			$this->db = new PDO('mysql:host='.TEST_DB_HOST.';dbname='.TEST_DB_NAME.';charset=utf8', TEST_DB_USER, TEST_DB_PWD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)); 
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
		
		$stmt = $this->db->query('SELECT * FROM book ORDER BY id');
			
		while($row = $stmt->fetch(PDO::FETCH_ASSOC))
		{
			$booklist[] = new Book($row['title'],$row['author'],$row['description'],$row['id']);
			//$booklist[] = $row['id'] . ': ' . $row['title'].':'.$row['author'].':'.$row['description'];
		}

        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		
		$stmt = $this->db->prepare('SELECT * FROM book WHERE id=:id');
		$stmt->bindValue(':id', $id);
		$stmt->execute();

		$book = $stmt->fetch(PDO::FETCH_ASSOC);
		if($book){
			return new Book($book['title'],$book['author'],$book['description'],$book['id']);
		}
		else return null;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		if(($book->title == "") || ($book->author =="")){  //checks for empty fields
			throw new Exception('Invalid input.');
		}
		if (is_a($book, 'Book')){
			
			$stmt = $this->db->prepare('INSERT INTO book (title, author, description) VALUES(:title, :author, :description)');
			$stmt->bindValue(':title', $book->title);
			$stmt->bindValue(':author', $book->author);
			$stmt->bindValue(':description', $book->description);
			
			$stmt->execute();
			
			$book->id = $this->db->lastInsertId(); //sets id into book object
		}
		
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     */
    public function modifyBook($book)
    {
		if(($book->title == "") || ($book->author =="")){
			throw new Exception('Invalid input.');
		}
		if (is_a($book, 'Book')){
		$stmt = $this->db->prepare('UPDATE book SET title=:title, author=:author, description=:description WHERE id=:id  ');
		$stmt->bindValue(':title', $book->title);
		$stmt->bindValue(':author', $book->author);
		$stmt->bindValue(':description', $book->description);
		$stmt->bindValue(':id', $book->id);
		$stmt->execute();
		}
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		$stmt = $this->db->prepare('DELETE FROM book WHERE id=:id');
		$stmt->bindValue(':id', $id);
		$stmt->execute();
    }
	
}
?>